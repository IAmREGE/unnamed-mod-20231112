package rege.rege.minecraftmod.unnamed_20231112.server.command;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.DynamicCommandExceptionType;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;

import org.apache.commons.lang3.StringEscapeUtils;

import org.jetbrains.annotations.Nullable;

import rege.rege.minecraftmod.unnamed_20231112.block.CharacterBoardBlock;

public class Unnamed_mod_20231112Command {
    public static final DynamicCommandExceptionType FAILED_ENTITY_EXCEPTION =
    new DynamicCommandExceptionType(entityName -> Text.translatable(
        "commands.unnamed_mod_20231112.failed.entity", entityName
    ));
    public static final DynamicCommandExceptionType FAILED_ITEMLESS_EXCEPTION =
    new DynamicCommandExceptionType(entityName -> Text.translatable(
        "commands.unnamed_mod_20231112.failed.itemless", entityName
    ));
    public static final DynamicCommandExceptionType
    FAILED_INCOMPATIBLE_EXCEPTION =
    new DynamicCommandExceptionType(itemName -> Text.translatable(
        "commands.unnamed_mod_20231112.failed.incompatible", itemName
    ));
    public static final SimpleCommandExceptionType FAILED_EXCEPTION =
    new SimpleCommandExceptionType(Text.translatable(
        "commands.unnamed_mod_20231112.failed"
    ));
    public static final SimpleCommandExceptionType INVALID_STRING_EXCEPTION =
    new SimpleCommandExceptionType(Text.translatable(
        "commands.unnamed_mod_20231112.invalid_string"
    ));

    public Unnamed_mod_20231112Command() {}

    public static void
    register(CommandDispatcher<ServerCommandSource> dispatcher) {
        dispatcher.register(
            CommandManager.literal("unnamed_mod_20231112").executes(
                context -> execute(context.getSource(), null)
            ).then(CommandManager.argument(
                "escaped_string", StringArgumentType.greedyString()
            ).executes(context -> execute(
                context.getSource(),
                context.getArgument("escaped_string", String.class)
            )))
        );
    }

    private static int
    execute(ServerCommandSource source, @Nullable String text)
    throws CommandSyntaxException {
        if (text != null) {
            try {
                if ((text = StringEscapeUtils.unescapeJson(text)) == null) {
                    throw INVALID_STRING_EXCEPTION.create();
                }
            } catch (IllegalArgumentException e) {
                throw INVALID_STRING_EXCEPTION.create();
            }
        }
        final Entity ETT = source.getEntity();
        if (ETT != null) {
            if (ETT instanceof LivingEntity) {
                final LivingEntity LETT = (LivingEntity)ETT;
                final ItemStack ITMSTK = LETT.getMainHandStack();
                if (!ITMSTK.isEmpty()) {
                    if ((ITMSTK.getItem() instanceof BlockItem) &&
                        (((BlockItem)(ITMSTK.getItem())).getBlock() instanceof
                         CharacterBoardBlock)) {
                        ITMSTK
                        .setCustomName((text == null) ? null : Text.of(text));
                    } else {
                        throw FAILED_INCOMPATIBLE_EXCEPTION.create(
                            ITMSTK.getItem().getName(ITMSTK).getString()
                        );
                    }
                } else {
                    throw FAILED_ITEMLESS_EXCEPTION
                          .create(LETT.getName().getString());
                }
            } else {
                throw FAILED_ENTITY_EXCEPTION
                      .create(ETT.getName().getString());
            }
        } else {
            throw FAILED_EXCEPTION.create();
        }
        if (text == null) {
            source.sendFeedback(() -> Text.translatable(
                "commands.unnamed_mod_20231112.success.remove"
            ), true);
        } else {
            source.sendFeedback(() -> Text.translatable(
                "commands.unnamed_mod_20231112.success.apply"
            ), true);
        }
        return 1;
    }
}
