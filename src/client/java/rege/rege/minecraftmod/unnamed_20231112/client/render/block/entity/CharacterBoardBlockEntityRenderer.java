package rege.rege.minecraftmod.unnamed_20231112.client.render.block.entity;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.BlockState;
import net.minecraft.client.font.TextRenderer;
import net.minecraft.client.font.TextRenderer.TextLayerType;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.render.block.entity.BlockEntityRendererFactory;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.OrderedText;
import net.minecraft.text.Text;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RotationAxis;
import net.minecraft.util.math.Vec3d;

import rege.rege.minecraftmod.unnamed_20231112.block.entity
       .CharacterBoardBlockEntity;

import static net.minecraft.state.property.Properties.HORIZONTAL_FACING;

@Environment(EnvType.CLIENT)
public class CharacterBoardBlockEntityRenderer
implements BlockEntityRenderer<CharacterBoardBlockEntity> {
    public static final Vec3d TEXT_OFFSET = new Vec3d(0., 0.375, 0.075);

    private final TextRenderer tr;

    public
    CharacterBoardBlockEntityRenderer(BlockEntityRendererFactory.Context ctx) {
        this.tr = ctx.getTextRenderer();
    }

    @Override
    public void render(CharacterBoardBlockEntity entity, float tickDelta,
                       MatrixStack matrices,
                       VertexConsumerProvider vertexConsumers, int light,
                       int overlay) {
        final Text TX = entity.getCustomName();
        if (TX == null) {
            return;
        }
        BlockState st = entity.getCachedState();
        matrices.push();
        this.setAngles(matrices, -st.get(HORIZONTAL_FACING).asRotation());
        this.renderText(entity.getPos(), TX, matrices, vertexConsumers, light,
                        8, 20);
        matrices.pop();
    }

    void setAngles(MatrixStack matrices, float rotationDegrees) {
        matrices.translate(0.5f, 2.25f, 0.5f);
        matrices
        .multiply(RotationAxis.POSITIVE_Y.rotationDegrees(rotationDegrees));
        matrices.translate(0f, -0.3125f, -0.4375f);
    }

    void renderText(BlockPos pos, Text tx, MatrixStack matrices,
                    VertexConsumerProvider vertexConsumers, int light,
                    int lineHeight, int lineWidth) {
        matrices.push();
        this.setTextAngles(matrices, TEXT_OFFSET);
        int i = 16777215;
        int j = 4 * lineHeight / 2;
        int k = i;
        int l = light;
        OrderedText odt=tx.asOrderedText();
        this.tr.draw(odt, (float)(-tr.getWidth(odt) / 2), (float)j, k, false,
                     matrices.peek().getPositionMatrix(), vertexConsumers,
                     TextLayerType.POLYGON_OFFSET, 0, l);
        matrices.pop();
    }

    private void setTextAngles(MatrixStack matrices, Vec3d translation) {
        final float F = 0.09375f;
        matrices.translate(translation.x, translation.y, translation.z);
        matrices.scale(F, -F, F);
    }
}
