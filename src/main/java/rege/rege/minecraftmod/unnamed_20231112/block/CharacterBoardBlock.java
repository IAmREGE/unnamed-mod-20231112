package rege.rege.minecraftmod.unnamed_20231112.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.ShapeContext;
import net.minecraft.block.WallMountedBlock;
import net.minecraft.block.Waterloggable;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.enums.BlockFace;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.FluidState;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.StateManager.Builder;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.state.property.Properties;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;

import rege.rege.minecraftmod.unnamed_20231112.Unnamed_20231112Main;
import rege.rege.minecraftmod.unnamed_20231112.block.entity
       .CharacterBoardBlockEntity;

import static net.minecraft.fluid.Fluids.WATER;
import static net.minecraft.util.shape.VoxelShapes.cuboid;
import static net.minecraft.util.shape.VoxelShapes.empty;

public class CharacterBoardBlock extends WallMountedBlock
implements Waterloggable, BlockEntityProvider {
    public static final BooleanProperty WATERLOGGED = Properties.WATERLOGGED;
    public static final VoxelShape FACING_DOWN =
    cuboid(0f, 0.9375f, 0f, 1f, 1f, 1f);
    public static final VoxelShape FACING_UP =
    cuboid(0f, 0f, 0f, 1f, 0.0625f, 1f);
    public static final VoxelShape FACING_NORTH =
    cuboid(0f, 0f, 0.9375f, 1f, 1f, 1f);
    public static final VoxelShape FACING_SOUTH =
    cuboid(0f, 0f, 0f, 1f, 1f, 0.0625f);
    public static final VoxelShape FACING_WEST =
    cuboid(0.9375f, 0f, 0f, 1f, 1f, 1f);
    public static final VoxelShape FACING_EAST =
    cuboid(0f, 0f, 0f, 0.0625f, 1f, 1f);

    public CharacterBoardBlock(Settings settings) {
        super(settings);
        this.setDefaultState(this.getDefaultState().with(FACE, BlockFace.WALL)
                             .with(FACING, Direction.SOUTH)
                             .with(WATERLOGGED, false));
    }

    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new CharacterBoardBlockEntity(pos, state);
    }

    @Override
    public VoxelShape getOutlineShape(BlockState state, BlockView view,
                                      BlockPos pos, ShapeContext context) {
        switch (state.get(FACING)) {
            case NORTH: return FACING_NORTH;
            case SOUTH: return FACING_SOUTH;
            case WEST: return FACING_WEST;
            case EAST: return FACING_EAST;
            default: assert false;
        }
        return null;
    }

    @Override
    public VoxelShape getCollisionShape(BlockState state, BlockView view,
                                        BlockPos pos, ShapeContext context) {
        return Unnamed_20231112Main.isCharacterBoardCollision() ?
               this.getOutlineShape(state, view, pos, context) : empty();
    }

    @Override
    public VoxelShape getCullingShape(BlockState state, BlockView view,
                                      BlockPos pos) {
        return Unnamed_20231112Main.isCharacterBoardCulling() ?
               this.getOutlineShape(state, view, pos, null) : empty();
    }

    @Override
    public VoxelShape getSidesShape(BlockState state, BlockView view,
                                    BlockPos pos) {
        return Unnamed_20231112Main.isCharacterBoardSides() ?
               this.getOutlineShape(state, view, pos, null) : empty();
    }

    @Override
    public BlockState getStateForNeighborUpdate(
        BlockState state, Direction direction, BlockState neighborState,
        WorldAccess world, BlockPos pos, BlockPos neighborPos
    ) {
        if (state.get(WATERLOGGED).booleanValue()) {
            world.scheduleFluidTick(pos, WATER, WATER.getTickRate(world));
        }
        return state;
    }

    @Override
    public FluidState getFluidState(BlockState state) {
        return state.get(WATERLOGGED).booleanValue() ? WATER.getStill(false) :
               super.getFluidState(state);
    }

    @Override
    public boolean isTransparent(BlockState state, BlockView world, BlockPos pos) {
        return !Unnamed_20231112Main.isCharacterBoardCulling();
    }

    @Override
    protected void appendProperties(Builder<Block, BlockState> builder) {
        builder.add(FACE, FACING, WATERLOGGED);
    }

    @Override
    public void onPlaced(World world, BlockPos pos, BlockState state,
                         LivingEntity placer, ItemStack itemStack) {
        if (world.isClient) {
            world
            .getBlockEntity(pos, Unnamed_20231112Main.CHARACTER_BOARD)
            .ifPresent(be -> be.setCustomName(itemStack.hasCustomName() ?
                             itemStack.getName() : null));
        } else if (itemStack.hasCustomName()) {
            world
            .getBlockEntity(pos, Unnamed_20231112Main.CHARACTER_BOARD)
            .ifPresent(be -> be.setCustomName(itemStack.getName()));
        }
    }

    @Override
    public
    ItemStack getPickStack(BlockView view, BlockPos pos, BlockState state) {
        BlockEntity be = view.getBlockEntity(pos);
        return (be instanceof CharacterBoardBlockEntity) ?
               new ItemStack(this.asItem())
               .setCustomName(((CharacterBoardBlockEntity)be).getCustomName()):
               super.getPickStack(view, pos, state);
    }

    @Override
    public BlockState getPlacementState(ItemPlacementContext ctx) {
        final BlockState PSTATE = super.getPlacementState(ctx);
        return (PSTATE != null) ? PSTATE.with(FACE, BlockFace.WALL).with(
            WATERLOGGED,
            Unnamed_20231112Main.isCharacterBoardActiveWaterloggable() &&
            ctx.getWorld().getFluidState(ctx.getBlockPos()).getFluid() == WATER
        ) : null;
    }

    @Override
    public boolean canFillWithFluid(PlayerEntity player, BlockView world,
                                    BlockPos pos, BlockState state,
                                    Fluid fluid) {
        return Unnamed_20231112Main.isCharacterBoardActiveWaterloggable() &&
               Waterloggable.super
               .canFillWithFluid(player, world, pos, state, fluid);
    }

    @Override
    public boolean tryFillWithFluid(WorldAccess world, BlockPos pos,
                                    BlockState state, FluidState fluidState) {
        return Unnamed_20231112Main.isCharacterBoardActiveWaterloggable() &&
               Waterloggable.super
               .tryFillWithFluid(world, pos, state, fluidState);
    }

    @Override
    public ItemStack tryDrainFluid(PlayerEntity player, WorldAccess world,
                                   BlockPos pos, BlockState state) {
        return Unnamed_20231112Main.isCharacterBoardActiveWaterloggable() ?
               Waterloggable.super.tryDrainFluid(player, world, pos, state) :
               ItemStack.EMPTY;
    }
}
