package rege.rege.minecraftmod.unnamed_20231112.block.entity;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.listener.ClientPlayPacketListener;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.s2c.play.BlockEntityUpdateS2CPacket;
import net.minecraft.text.Text;
import net.minecraft.util.Nameable;
import net.minecraft.util.math.BlockPos;

import org.jetbrains.annotations.Nullable;

import static net.minecraft.nbt.NbtElement.STRING_TYPE;

import static rege.rege.minecraftmod.unnamed_20231112.Unnamed_20231112Main
              .CHARACTER_BOARD;

public class CharacterBoardBlockEntity extends BlockEntity implements Nameable{
    @Nullable
    private Text customName;

    public CharacterBoardBlockEntity(BlockPos pos, BlockState state) {
        super(CHARACTER_BOARD, pos, state);
    }

    @Override
    public Text getName() {
        return (this.customName != null) ? this.customName :
               Text.translatable("block.kwaicoloncyy.character_board");
    }

    @Override
    public Text getCustomName() {
       return this.customName;
    }

    public void setCustomName(Text customName) {
        this.customName = customName;
    }

    @Override
    protected void writeNbt(NbtCompound nbt) {
        super.writeNbt(nbt);
        if (this.customName != null) {
            nbt
            .putString("CustomName", Text.Serializer.toJson(this.customName));
        }
    }

    @Override
    public void readNbt(NbtCompound nbt) {
        super.readNbt(nbt);
        if (nbt.contains("CustomName", STRING_TYPE)) {
            this.customName =
            Text.Serializer.fromJson(nbt.getString("CustomName"));
        }
    }

    @Override
    public Packet<ClientPlayPacketListener> toUpdatePacket() {
        return BlockEntityUpdateS2CPacket.create(this);
    }

    @Override
    public NbtCompound toInitialChunkDataNbt() {
        return this.createNbt();
    }
}
