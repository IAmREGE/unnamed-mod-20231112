package rege.rege.minecraftmod.unnamed_20231112;

import net.fabricmc.api.ModInitializer;

import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback;
import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroup;
import net.fabricmc.fabric.api.object.builder.v1.block.entity
       .FabricBlockEntityTypeBuilder;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.Rarity;
import net.minecraft.text.Text;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;

import org.slf4j.Logger;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import rege.rege.minecraftmod.unnamed_20231112.block.CharacterBoardBlock;
import rege.rege.minecraftmod.unnamed_20231112.block.entity
       .CharacterBoardBlockEntity;
import rege.rege.minecraftmod.unnamed_20231112.server.command
       .Unnamed_mod_20231112Command;

import static org.slf4j.LoggerFactory.getLogger;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Unnamed_20231112Main implements ModInitializer {
    // This logger is used to write text to the console and the log file.
    // It is considered best practice to use your mod id as the logger's name.
    // That way, it's clear which mod wrote info, warnings, and errors.

    public static final Logger LOGGER = getLogger("rege-unnamed-mod-20231112");
    public static final CharacterBoardBlock CHARACTER_BOARD_0 =
    new CharacterBoardBlock(AbstractBlock.Settings.create().strength(3f)
                            .allowsSpawning((a, b, c, d) -> false));
    public static final CharacterBoardBlock CHARACTER_BOARD_1 =
	new CharacterBoardBlock(AbstractBlock.Settings.create().strength(5f)
							.allowsSpawning((a, b, c, d) -> false));
    public static final CharacterBoardBlock CHARACTER_BOARD_2 =
	new CharacterBoardBlock(AbstractBlock.Settings.create().strength(10f)
							.allowsSpawning((a, b, c, d) -> false));
    public static final CharacterBoardBlock CHARACTER_BOARD_3 =
	new CharacterBoardBlock(AbstractBlock.Settings.create().strength(15f)
							.allowsSpawning((a, b, c, d) -> false));
    public static final CharacterBoardBlock CHARACTER_BOARD_4 =
	new CharacterBoardBlock(AbstractBlock.Settings.create().strength(15f)
							.allowsSpawning((a, b, c, d) -> false));
    public static final CharacterBoardBlock CHARACTER_BOARD_5 =
	new CharacterBoardBlock(AbstractBlock.Settings.create().strength(12f)
							.allowsSpawning((a, b, c, d) -> false));
    public static final BlockEntityType<CharacterBoardBlockEntity>
    CHARACTER_BOARD = FabricBlockEntityTypeBuilder.create(
        CharacterBoardBlockEntity::new, CHARACTER_BOARD_0, CHARACTER_BOARD_1,
        CHARACTER_BOARD_2, CHARACTER_BOARD_3, CHARACTER_BOARD_4,
        CHARACTER_BOARD_5
    ).build();
    public static final BlockItem CHARACTER_BOARD_0_ITEM =
    new BlockItem(CHARACTER_BOARD_0, new Item.Settings().maxCount(64));
    public static final BlockItem CHARACTER_BOARD_1_ITEM =
    new BlockItem(CHARACTER_BOARD_1, new Item.Settings().maxCount(64));
    public static final BlockItem CHARACTER_BOARD_2_ITEM =
    new BlockItem(CHARACTER_BOARD_2, new Item.Settings().maxCount(64));
    public static final BlockItem CHARACTER_BOARD_3_ITEM =
    new BlockItem(CHARACTER_BOARD_3, new Item.Settings().maxCount(64));
    public static final BlockItem CHARACTER_BOARD_4_ITEM =
    new BlockItem(CHARACTER_BOARD_4, new Item.Settings().maxCount(64));
    public static final BlockItem CHARACTER_BOARD_5_ITEM =
    new BlockItem(CHARACTER_BOARD_5,
                  new Item.Settings().maxCount(64).rarity(Rarity.EPIC));
    private static final ItemGroup ITMGRP = FabricItemGroup.builder().icon(
        () -> new ItemStack(CHARACTER_BOARD_0_ITEM)
    ).displayName(Text.literal("Unnamed mod 20231112")).entries((ctx, ent) -> {
        ent.add(CHARACTER_BOARD_0_ITEM);
        ent.add(CHARACTER_BOARD_1_ITEM);
        ent.add(CHARACTER_BOARD_2_ITEM);
        ent.add(CHARACTER_BOARD_3_ITEM);
        ent.add(CHARACTER_BOARD_4_ITEM);
        ent.add(CHARACTER_BOARD_5_ITEM);
    }).build();

    private static boolean characterBoardActiveWaterloggable = false;
    private static boolean characterBoardCollision = false;
    private static boolean characterBoardCulling = false;
    private static boolean characterBoardSides = false;

    public static boolean isCharacterBoardActiveWaterloggable() {
        return characterBoardActiveWaterloggable;
    }

    public static boolean isCharacterBoardCollision() {
        return characterBoardCollision;
    }

    public static boolean isCharacterBoardCulling() {
        return characterBoardCulling;
    }

    public static boolean isCharacterBoardSides() {
        return characterBoardSides;
    }

    @Override
    public void onInitialize() {
        // This code runs as soon as Minecraft is in a mod-load-ready state.
        // However, some things (like resources) may still be uninitialized.
        // Proceed with mild caution.

        LOGGER.info("This shouldn't be end...");
        try {
            final FileReader FR =
            new FileReader("config/unnamed_mod_20231112.json");
            try {
                final StringBuilder SB = new StringBuilder();
                int a;
                while ((a = FR.read()) != -1) {
                    SB.append((char)a);
                }
                final JsonElement ELE = new JsonParser().parse(SB.toString());
                if (ELE.isJsonObject()) {
                    final JsonObject JSON = ELE.getAsJsonObject();
                    if (JSON.has("character_board_active_waterloggable")) {
                        final JsonElement V =
                        JSON.get("character_board_active_waterloggable");
                        if (V.isJsonPrimitive()) {
                            characterBoardActiveWaterloggable =
                            V.getAsBoolean();
                        }
                    }
                    if (JSON.has("character_board_collision")) {
                        final JsonElement V =
                        JSON.get("character_board_collision");
                        if (V.isJsonPrimitive()) {
                            characterBoardCollision = V.getAsBoolean();
                        }
                    }
                    if (JSON.has("character_board_culling")) {
                        final JsonElement V =
                        JSON.get("character_board_culling");
                        if (V.isJsonPrimitive()) {
                            characterBoardCulling = V.getAsBoolean();
                        }
                    }
                    if (JSON.has("character_board_sides")) {
                        final JsonElement V =
                        JSON.get("character_board_sides");
                        if (V.isJsonPrimitive()) {
                            characterBoardSides = V.getAsBoolean();
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (IllegalStateException e) {}
        } catch (FileNotFoundException e) {}
        Registry.register(Registries.BLOCK, new Identifier("kwaicoloncyy", "character_board_0"), CHARACTER_BOARD_0);
        Registry.register(Registries.BLOCK, new Identifier("kwaicoloncyy", "character_board_1"), CHARACTER_BOARD_1);
        Registry.register(Registries.BLOCK, new Identifier("kwaicoloncyy", "character_board_2"), CHARACTER_BOARD_2);
        Registry.register(Registries.BLOCK, new Identifier("kwaicoloncyy", "character_board_3"), CHARACTER_BOARD_3);
        Registry.register(Registries.BLOCK, new Identifier("kwaicoloncyy", "character_board_4"), CHARACTER_BOARD_4);
        Registry.register(Registries.BLOCK, new Identifier("kwaicoloncyy", "character_board_5"), CHARACTER_BOARD_5);
        Registry.register(Registries.BLOCK_ENTITY_TYPE, new Identifier("kwaicoloncyy", "character_board"), CHARACTER_BOARD);
        Registry.register(Registries.ITEM, new Identifier("kwaicoloncyy", "character_board_0"), CHARACTER_BOARD_0_ITEM);
        Registry.register(Registries.ITEM, new Identifier("kwaicoloncyy", "character_board_1"), CHARACTER_BOARD_1_ITEM);
        Registry.register(Registries.ITEM, new Identifier("kwaicoloncyy", "character_board_2"), CHARACTER_BOARD_2_ITEM);
        Registry.register(Registries.ITEM, new Identifier("kwaicoloncyy", "character_board_3"), CHARACTER_BOARD_3_ITEM);
        Registry.register(Registries.ITEM, new Identifier("kwaicoloncyy", "character_board_4"), CHARACTER_BOARD_4_ITEM);
        Registry.register(Registries.ITEM, new Identifier("kwaicoloncyy", "character_board_5"), CHARACTER_BOARD_5_ITEM);
        Registry.register(Registries.ITEM_GROUP, new Identifier("kwaicoloncyy", "unnamed_mod_20231112"), ITMGRP);
        CommandRegistrationCallback.EVENT.register(
            (dispatcher, registryAccess, environment) -> {
                Unnamed_mod_20231112Command.register(dispatcher);
            }
        );
    }
}
