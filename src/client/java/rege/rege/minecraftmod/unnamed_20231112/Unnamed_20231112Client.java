package rege.rege.minecraftmod.unnamed_20231112;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.rendereregistry.v1
       .BlockEntityRendererRegistry;

import rege.rege.minecraftmod.unnamed_20231112.client.render.block.entity
       .CharacterBoardBlockEntityRenderer;

public class Unnamed_20231112Client implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        // This entrypoint is suitable for setting up client-specific logic, such as rendering.
        BlockEntityRendererRegistry.INSTANCE
        .register(Unnamed_20231112Main.CHARACTER_BOARD,
                  CharacterBoardBlockEntityRenderer::new);
    }
}